$(() => {
  let currentQuestion = 0;
  let correct = 0;
  let currentSelection = -1;
  let $question = $('.question');
  let $choices = $('.choices');
  let $choice = $('.choice');
  let $next = $('.next');
  let $result = $('.result');
  let questionData = [];
  $.get('./questions.json', (data) => {
    questionData = data;
    renderQuestion(currentQuestion);
    renderChoices(currentQuestion);
    addEvents();
  });

  const doneQuiz = () => {
    $question.text('');
    $choices.empty();
    $result.text(`Correct ${correct} | Incorrect ${questionData.length - correct}`);
  };

  const setNextQuestion = (e) => {
    currentQuestion++;
    if(currentQuestion === questionData.length - 1) {
      $next.text('Finish')
    } else if(currentQuestion > questionData.length - 1) {
      doneQuiz();
      currentQuestion = 0;
      renderQuestion();
      renderChoices();
    }
    $result.text('');
    renderQuestion(currentQuestion);
    renderChoices(currentQuestion);
  };

  const choiceSelected = (e) => {
    $link = $(e.target);
    currentSelection = $link.closest(".choice").index();
    setSelectedAnswer($link)
    validateAnswer(currentQuestion, currentSelection);
    showNextButton();
  };

  const setSelectedAnswer = ($link) => {
    $('.link')
      .removeClass('selected')
      .attr('disabled', 'disabled');
    $link.addClass('selected');
  };

  const showNextButton = () => {
    $('.next').show();
  };

  const validateAnswer = (currentQuestion, currentSelection) => {
    if(questionData[currentQuestion].a === currentSelection) {
      correct++;
      $result.text("You are right!");
    } else {
      $result.text("You are incorrect! FOOL!");
    }
  };

  const addEvents = () => {
    $choices.on('click', ".link", choiceSelected);
    $next.on('click', setNextQuestion);
  };

  const renderQuestion = (questionNumber) => {
    $question.text(questionData[questionNumber].q);
  };

  const renderChoices = (questionNumber) => {
    $choices.empty();
    const choices = questionData[questionNumber].choices;
    choices.forEach(choice => {
      $newChoice = $choice
        .find('.link')
        .text(choice)
        .end()
        .clone();
      $choices.append($choice.clone());
    });
  };

});